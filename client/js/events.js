"use strict";

Template.form.events({
    'submit form': function (event) {
        //keeps page from refreshing
        event.preventDefault();

        var imageFile = event.currentTarget.children[2].files[0];
        var nameText = event.currentTarget.children[0].value;
        var messageText = event.currentTarget.children[1].value;
        //if nothing is typed in input, than a popup notifies to write something
        if (nameText === "") {
            Materialize.toast("Please input your name before submitting.", 4000);
            return false;
        }
        if (messageText === "") {
            Materialize.toast("Please write a message before submitting.", 4000);
            return false;
        }
        //         insert task into todo list
        Collections.Images.insert(imageFile, function (error, fileObject) {
            if (error) {
                Materialize.toast("Image failed to send.", 4000);
            } else {
                Collections.Posts.insert({
                    name: nameText,
                    createdAt: new Date(),
                    message: messageText,
                    id: fileObject._id
                });
                //submit post data to databasts
                //author, message, created by date, image id: fileObject._id
                $('.grid').masonry('reloadItems');
            }

        });


        //        event.currentTarget.children[0].firstElementChild.value = "";
        return false;
    }
});